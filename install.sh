#!/bin/bash

PS3='Please enter your choice: '
options=("Install lightdm" "Install wm" "Install Apps" "Quit")
select opt in "${options[@]}"
do
	case $opt in
		"Install lightdm")
			sudo pacman -S lightdm xorg-server lightdm-gtk-greeter
			sudo systemctl enable lightdm
			;;
		"Install wm")
			sudo pacman -S i3-gaps rofi
			ln -sf ~/.config/i3/config ./wm/i3/config
			ln -sf ~/.config/rofi/config.rasi ./wm/rofi/config.rasi
			;;
		"Install Apps")
			sudo pacman -S neofetch alacritty
			ln -sf ~/.config/alacritty/alacritty.yml ./apps/alacritty/alacritty.yml
			ln -sf ~/.config/neofetch/config.conf ./apps/neofetch/config.conf
			;;
		"Quit")
			break
			;;
		*) echo "invalid option $REPLY";;
	esac
done
